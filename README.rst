Score Keeping for Pool (9 ball)
==========================================================

Project Description:
--------------------

Playing 9 ball in a league for a bit, I was surprised we still kept score with pencil and paper.  This is my attempt to fix that.  Or at least provide an outlet for me to mess around with Docker.  And try out Kubernetes.  And React.js.  And Django REST Framework.  And GitLab's Pipeline thing.  And bumpversion.  And just be a test project for that sort of thing.
