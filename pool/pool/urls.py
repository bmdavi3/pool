from django.conf.urls import url, include
from rest_framework import routers

from scoring import views


router = routers.DefaultRouter()
router.register(r'matches', views.MatchViewSet)
router.register(r'players', views.PlayerViewSet)
router.register(r'skill-levels', views.SkillLevelViewSet)
router.register(r'shots', views.ShotViewSet)
router.register(r'actions', views.ActionViewSet)
router.register(r'balls', views.BallViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^score-match/(?P<match_id>\d+)/$', views.score_match),
    url(r'^score-match-with-react/$', views.score_match_with_react)
]
