from django.contrib.staticfiles.storage import ManifestFilesMixin
from storages.backends.s3boto import S3BotoStorage

from pool import __version__


class S3ManifestStorage(ManifestFilesMixin, S3BotoStorage):
    @property
    def manifest_name(self):
        filename = 'staticfiles-{version}.json'
        return filename.format(version=__version__)
