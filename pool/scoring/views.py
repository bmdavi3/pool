from django.shortcuts import render
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, mixins

from scoring.models import Match, Player, SkillLevel, Shot, Action, Ball
from scoring.serializers import MatchSerializer, PlayerSerializer, \
    SkillLevelSerializer, ShotSerializer, ActionSerializer, BallSerializer


def score_match(request, match_id=None):
    match = get_object_or_404(Match, id=match_id)

    balls_left = match.get_balls_left()

    balls = [(x, x not in balls_left) for x in range(1, 10)]

    context = {
        'player_1': match.player_1,
        'player_2': match.player_2,
        'next_up': match.get_next_up(),
        'balls': balls,
    }

    return render(request, 'scoring/score_match.html', context)


def score_match_with_react(request):
    return render(request, 'scoring/score_match_with_react.html')


class MatchViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows matches to be viewed or edited.
    """
    queryset = Match.objects.all()
    serializer_class = MatchSerializer


class PlayerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows players to be viewed or edited.
    """
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer


class SkillLevelViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows skill levels to be viewed or edited.
    """
    queryset = SkillLevel.objects.all()
    serializer_class = SkillLevelSerializer


class ShotViewSet(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    """
    API endpoint that allows shots to be created.
    We will probably chop out most of the above mixins, but they're useful for now
    """
    queryset = Shot.objects.all()
    serializer_class = ShotSerializer


class ActionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows skill levels to be viewed or edited.
    """
    queryset = Action.objects.all()
    serializer_class = ActionSerializer


class BallViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows skill levels to be viewed or edited.
    """
    queryset = Ball.objects.all()
    serializer_class = BallSerializer
