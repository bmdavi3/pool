var data = [];

var CurrentShooterDisplay = React.createClass({
  render: function() {
    return (
      <div className="currentShooterName">
        Current Player: {this.props.name}
      </div>
    );
  }
});

var NineBallGrid = React.createClass({
  render: function() {

    var ball_rows = [];

    for (var i = 0; i < 3; i++) {
      var row = [];
      for (var j = 0; j < 3; j++) {
        var ball_number = i * 3 + j + 1;
        var sunk = false;

        if (this.props.sunk_balls.indexOf(ball_number) >= 0) {
          sunk = true;
        }
        row.push([ball_number, sunk]);
      }

      ball_rows.push(row);
    }

    return (
      <div className="ballGrid">
        {ball_rows.map(function(ball_row, i) {
          return <BallRow key={i} ball_row={ball_row} />;
        })}
      </div>
    );
  }
});

var BallRow = React.createClass({
  render: function() {
    var ball_row = this.props.ball_row;

    return (
      <ReactBootstrap.ButtonToolbar>
        {ball_row.map(function(ball, i) {
          return <Ball key={ball[0]} ball={ball[0]} sunk={ball[1]} />;
        })}
      </ReactBootstrap.ButtonToolbar>
    );
  }
});

var Ball = React.createClass({
  render: function() {
    return (
      <ReactBootstrap.Button disabled={this.props.sunk}>
        {this.props.ball}
      </ReactBootstrap.Button>
    );
  }
});

var ActionRow = React.createClass({
  render: function() {
    var actions = this.props.actions;

    return (
      <ReactBootstrap.ButtonToolbar>
        {actions.map(function(action) {
          return <Action key={action} name={action} />;
        })}
      </ReactBootstrap.ButtonToolbar>
    );
  }
});

var Action = React.createClass({
  render: function() {
    return (
      <ReactBootstrap.Button>
        {this.props.name}
      </ReactBootstrap.Button>
    );
  }
});

var Game = React.createClass({
  getInitialState: function() {
    return {
      sunk_balls: [],
      current_player_name: ''
    };
  },

  render: function() {
    var actions = ["Make", "Miss", "Foul", "Safety"];

    return (
      <div className="game">
        <NineBallGrid sunk_balls={this.state.sunk_balls} />
        <ActionRow actions={actions} />
        <CurrentShooterDisplay name={this.state.current_player_name} />
      </div>
    );
  }
});
ReactDOM.render(
  <Game />,
  document.getElementById('content')
);
