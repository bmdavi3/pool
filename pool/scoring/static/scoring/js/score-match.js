$(function() {
    $( "#submit" ).button();
    $( "#format" ).buttonset();
    $( "#radio" ).buttonset();

    $("input.disabled").button( "option", "disabled", true );
});
