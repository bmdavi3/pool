from scoring.models import Match, Player, SkillLevel, Shot, Action, Ball
from rest_framework import serializers


class PlayerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Player


class SimplePlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = ('id',)


class MatchSerializer(serializers.ModelSerializer):
    player_1 = PlayerSerializer()
    player_2 = PlayerSerializer()
    balls_left = serializers.ListField(
        child=serializers.IntegerField(min_value=1, max_value=15), source="get_balls_left"
    )
    next_up = SimplePlayerSerializer(source="get_next_up")

    class Meta:
        model = Match


class SkillLevelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SkillLevel


class ShotSerializer(serializers.HyperlinkedModelSerializer):
    match = serializers.HyperlinkedRelatedField(queryset=Match.objects.all(), view_name='match-detail', write_only=True)

    class Meta:
        model = Shot
        fields = ('action', 'balls', 'match')

    def create(self, validated_data):
        match = Match.objects.get(id=validated_data['match'])

        action_string = validated_data['action'].action
        balls = [x.number for x in validated_data['balls']]

        shot = match.create_shot(action_string, balls)

        return shot


class ActionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Action


class BallSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ball
