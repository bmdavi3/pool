from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from scoring.models import Player, Match, SkillLevel


class Command(BaseCommand):

    def handle(self, *args, **options):
        brian_user = User.objects.create_user(
            'bdavis', 'the.brian.m.davis@gmail.com', 'bdavis_pass')
        eric_user = User.objects.create_user(
            'eburns', 'ericjosephburns@gmail.com', 'eburns_pass')

        brian = Player.objects.create(user=brian_user)
        eric = Player.objects.create(user=eric_user)

        sl_3 = SkillLevel.objects.get(level=3)
        sl_4 = SkillLevel.objects.get(level=4)

        Match.objects.create(player_1=brian, player_2=eric,
                             player_1_skill_level=sl_4, player_2_skill_level=sl_3)
