from django.core.management.base import BaseCommand
from scoring.models import Turn, Shot, Action, Match, Ball


class Command(BaseCommand):

    def handle(self, *args, **options):
        Turn.objects.all().delete()
        Shot.objects.all().delete()

        # Hey, first time after eye surgery!
        # [Player, action, ball]
        # 'k' = make
        # 'i' = miss
        # 's' = safety
        # 'f' = foul
        shots = [
            [0, 'k', [5]],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'k', [1]],
            [1, 'k', [2, 3]],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'k', [4]],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'f', []],
            [1, 'k', [6]],
            [1, 'k', [7]],
            [1, 'i', []],
            [0, 'k', [8]],
            [0, 'k', [9]],  # End game 1
            [0, 'i', []],
            [1, 'i', []],
            [0, 'f', []],
            [1, 'i', []],
            [0, 'f', []],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'k', [1]],
            [1, 'i', []],
            [0, 'i', []],
            [1, 's', []],
            [0, 'f', []],
            [1, 'i', []],
            [0, 'k', [2]],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'k', [3]],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'k', [4]],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'k', [5]],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'f', []],
            [1, 'k', [6]],
            [1, 'i', []],
            [0, 'k', [7]],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'k', [8]],
            [1, 's', []],
            [0, 'i', []],
            [1, 'i', []],
            [0, 'i', []],
            [1, 'k', [9]],  # End game 2
        ]

        match = Match.objects.all()[0]

        current_player = -1
        turn_number = 1

        actions = {
            'k': Action.objects.get(action="make"),
            'i': Action.objects.get(action="miss"),
            's': Action.objects.get(action="safety"),
            'f': Action.objects.get(action="foul"),
        }

        for s in shots:
            if s[0] != current_player:
                turn = Turn(number=turn_number, match=match)
                turn.save()
                turn_number += 1
                shot_number = 1
                current_player = s[0]

            action = actions[s[1]]

            shot = Shot(number=shot_number, turn=turn, action=action)
            shot.save()

            for number in s[2]:
                ball = Ball.objects.get(number=number)
                shot.balls.add(ball)

            shot_number += 1
