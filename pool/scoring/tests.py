from django.test import TestCase
from django.contrib.auth.models import User

from scoring.models import Match, SkillLevel, Turn, Action, Shot, Ball


class CreateShotTestCase(TestCase):
    def setUp(self):
        self.skill_level_1 = SkillLevel.objects.get(level=3)

        self.user_1 = User.objects.create_user(username='bdavis')
        self.user_2 = User.objects.create_user(username='eburns')

        self.match = Match.objects.create(
            user_1=self.user_1, user_2=self.user_2,
            user_1_skill_level=self.skill_level_1,
            user_2_skill_level=self.skill_level_1)

    def test_make_shot(self):
        self.assertEqual(Turn.objects.count(), 0)

        self.match.create_shot("make", [1])

        self.assertEqual(Turn.objects.count(), 1)
        self.assertEqual(Shot.objects.filter(number=1, action__action="make").count(), 1)

    def test_bad_input(self):
        with self.assertRaises(ValueError):
            self.match.create_shot("make", [])

        with self.assertRaises(ValueError):
            self.match.create_shot("miss", [1])


class GetOrCreateTurnTestCase(TestCase):
    def setUp(self):
        self.make = Action.objects.get(action="make")
        self.miss = Action.objects.get(action="miss")

        self.skill_level_1 = SkillLevel.objects.get(level=3)

        self.user_1 = User.objects.create_user(username='bdavis')
        self.user_2 = User.objects.create_user(username='eburns')

        self.match = Match.objects.create(
            user_1=self.user_1, user_2=self.user_2,
            user_1_skill_level=self.skill_level_1,
            user_2_skill_level=self.skill_level_1)

    def test_get_first_turn(self):
        self.assertEqual(Turn.objects.count(), 0)

        self.match.get_or_create_turn()

        self.assertEqual(Turn.objects.count(), 1)

        # This probably won't happen too often, but if a turn has been created
        # wo/ shots, it is still that turn
        self.match.get_or_create_turn()

        self.assertEqual(Turn.objects.count(), 1)

    def test_get_second_turn(self):
        turn = Turn.objects.create(number=1, match=self.match)
        shot_1 = Shot.objects.create(number=1, turn=turn, action=self.make)
        ball_1 = Ball.objects.get(number=1)
        shot_1.balls.add(ball_1)

        shot_2 = Shot.objects.create(number=2, turn=turn, action=self.make)
        ball_2 = Ball.objects.get(number=2)
        ball_3 = Ball.objects.get(number=3)
        shot_2.balls.add(ball_2)
        shot_2.balls.add(ball_3)

        shot_3 = Shot.objects.create(number=3, turn=turn, action=self.miss)

        self.match.get_or_create_turn()

        self.assertEqual(Turn.objects.count(), 2)


class GetNextUpTestCase(TestCase):
    def setUp(self):
        self.make = Action.objects.get(action="make")
        self.miss = Action.objects.get(action="miss")

        self.skill_level_1 = SkillLevel.objects.get(level=3)

        self.user_1 = User.objects.create_user(username='bdavis')
        self.user_2 = User.objects.create_user(username='eburns')

        self.match = Match.objects.create(
            user_1=self.user_1, user_2=self.user_2,
            user_1_skill_level=self.skill_level_1,
            user_2_skill_level=self.skill_level_1)

    def test_user_1_goes_first(self):
        next_up = self.match.get_next_up()

        self.assertEqual(next_up, self.user_1)

    def test_user_continues_after_make(self):
        turn = Turn.objects.create(number=1, match=self.match)
        Shot.objects.create(number=1, turn=turn, action=self.make)

        next_up = self.match.get_next_up()

        self.assertEqual(next_up, self.user_1)

    def test_user_yields_after_miss(self):
        turn = Turn.objects.create(number=1, match=self.match)
        Shot.objects.create(number=1, turn=turn, action=self.miss)

        next_up = self.match.get_next_up()

        self.assertEqual(next_up, self.user_2)


class GetBallsLeftTestCase(TestCase):
    def setUp(self):
        self.make = Action.objects.get(action="make")
        self.miss = Action.objects.get(action="miss")
        self.foul = Action.objects.get(action="foul")

        self.skill_level_1 = SkillLevel.objects.get(level=3)

        self.user_1 = User.objects.create_user(username='bdavis')
        self.user_2 = User.objects.create_user(username='eburns')

        self.match = Match.objects.create(
            user_1=self.user_1, user_2=self.user_2,
            user_1_skill_level=self.skill_level_1,
            user_2_skill_level=self.skill_level_1)

    def test_no_shots_taken(self):
        balls_left = self.match.get_balls_left()

        self.assertEqual(balls_left, [1, 2, 3, 4, 5, 6, 7, 8, 9])

    def test_some_misses_no_makes(self):
        turn = Turn.objects.create(number=1, match=self.match)
        Shot.objects.create(number=1, turn=turn, action=self.miss)

        turn = Turn.objects.create(number=2, match=self.match)
        Shot.objects.create(number=1, turn=turn, action=self.miss)

        balls_left = self.match.get_balls_left()

        self.assertEqual(balls_left, [1, 2, 3, 4, 5, 6, 7, 8, 9])

    def test_a_couple_makes(self):
        turn_1 = Turn.objects.create(number=1, match=self.match)
        shot_1 = Shot.objects.create(number=1, turn=turn_1, action=self.make)

        ball_7 = Ball.objects.get(number=7)
        ball_1 = Ball.objects.get(number=1)

        shot_1.balls.add(ball_7)
        shot_1.balls.add(ball_1)

        Shot.objects.create(number=2, turn=turn_1, action=self.miss)

        turn_2 = Turn.objects.create(number=2, match=self.match)
        shot_3 = Shot.objects.create(number=1, turn=turn_2, action=self.make)

        ball_2 = Ball.objects.get(number=2)

        shot_3.balls.add(ball_2)

        balls_left = self.match.get_balls_left()

        self.assertEqual(balls_left, [3, 4, 5, 6, 8, 9])

    def test_nine_made_along_with_another(self):
        turn = Turn.objects.create(number=1, match=self.match)
        shot = Shot.objects.create(number=1, turn=turn, action=self.make)

        ball_9 = Ball.objects.get(number=9)
        ball_1 = Ball.objects.get(number=1)

        shot.balls.add(ball_9)
        shot.balls.add(ball_1)

        balls_left = self.match.get_balls_left()

        self.assertEqual(balls_left, [1, 2, 3, 4, 5, 6, 7, 8, 9])

    def test_nine_made_on_foul(self):
        turn = Turn.objects.create(number=1, match=self.match)
        shot_1 = Shot.objects.create(number=1, turn=turn, action=self.make)

        ball_1 = Ball.objects.get(number=1)
        shot_1.balls.add(ball_1)

        shot_2 = Shot.objects.create(number=2, turn=turn, action=self.foul)
        ball_9 = Ball.objects.get(number=9)
        shot_2.balls.add(ball_9)

        balls_left = self.match.get_balls_left()

        self.assertEqual(balls_left, [2, 3, 4, 5, 6, 7, 8, 9])
