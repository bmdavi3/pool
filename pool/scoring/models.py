from django.contrib.auth.models import User
from django.db import models


class Ball(models.Model):
    number = models.IntegerField(unique=True)

    def __unicode__(self):
        return u'{}'.format(self.number)


class SkillLevel(models.Model):
    level = models.IntegerField(unique=True)
    handicap = models.IntegerField()

    def __unicode__(self):
        return u'Level: {}  Handicap: {}'.format(self.level, self.handicap)


class Player(models.Model):
    """
    New thinking on this.  Use the Player model when we need to record information about the user
    that isn't already on the User model, and that's it.  Foreign keys will point to the User
    model, not this model.  I think this is where my dislike of the two model auth scheme came
    from.  Trying to use the Player model for all foreign keys and relegating the User model to
    only handle Django auth specifc stuff.  Don't do that.
    """

    user = models.OneToOneField(User)

    def __unicode__(self):
        return u"{} {}".format(self.user.first_name, self.user.last_name)


class PlayerSkillLevel(models.Model):
    user = models.ForeignKey(User)
    skill_level = models.ForeignKey(SkillLevel)
    start_date = models.DateField()
    end_date = models.DateField()


class Match(models.Model):
    user_1 = models.ForeignKey(User, related_name='user_1_matches')
    user_2 = models.ForeignKey(User, related_name='user_2_matches')

    user_1_skill_level = models.ForeignKey(SkillLevel, related_name='+')
    user_2_skill_level = models.ForeignKey(SkillLevel, related_name='+')

    def __unicode__(self):
        return u"{} {} vs {} {}".format(self.user_1.first_name, self.user_1.last_name, self.user_2.first_name, self.user_2.last_name)

    def get_or_create_turn(self):
        if self.turn_set.exists():
            last_turn = self.turn_set.all().order_by('-number')[0]

            shots = last_turn.shot_set.all().order_by('-number')

            # If, for some reason, a turn has been created wo/ shots, return
            # that turn.  Same if the last shot was a make
            if len(shots) == 0 or shots[0].action.action == 'make':
                return last_turn

        # Otherwise, it's time for a new turn to be created
        new_number = self.turn_set.all().count() + 1

        new_turn = Turn.objects.create(number=new_number, match=self)

        return new_turn

    def create_shot(self, action_string, ball_numbers=None):
        """
        Creates the necessary Turns, Shots to record a shot.

        action_string: one of "make", "miss", "safety", "foul"
        ball_numbers: a list of balls sunk
        """

        # TODO: Prevent sinking balls already sunk

        if action_string == "make" and len(ball_numbers) < 1:
            raise ValueError("A make must specify which balls")

        if action_string == "miss" and len(ball_numbers) > 0:
            raise ValueError("A miss doesn't sink any balls")

        turn = self.get_or_create_turn()

        shot_number = turn.shot_set.all().count() + 1

        action = Action.objects.get(action=action_string)

        shot = Shot(turn=turn, number=shot_number, action=action)
        shot.save()

        for ball_number in ball_numbers:
            ball = Ball.objects.get(number=ball_number)
            shot.balls.add(ball)

        return shot

    def opponent(self, user):
        if user == self.user_1:
            return self.user_2

        return self.user_1

    def get_next_up(self):
        if not self.turn_set.exists():
            return self.user_1

        latest_turn = self.turn_set.all().order_by('-number')[0]
        latest_shot = latest_turn.shot_set.order_by('-number')[0]

        if self.turn_set.all().count() % 2 == 0:
            latest_user = self.user_2
        else:
            latest_user = self.user_1

        if latest_shot.action.action == 'make':
            return latest_user

        return self.opponent(latest_user)

    def get_balls_left(self):
        balls_left = range(1, 10)

        for turn in self.turn_set.all().order_by('number'):
            for shot in turn.shot_set.all().order_by('number'):
                # Must go through the balls in order, otherwise [9, 2, 3] would
                # reset to all 9 balls and then continue removing 2 and 3.
                balls_sunk = shot.balls.order_by('number').values_list('number', flat=True)

                for ball in balls_sunk:
                    if ball == 9:
                        if shot.action.action != 'foul':
                            balls_left = range(1, 10)
                    else:
                        balls_left.remove(ball)

        return balls_left


class Turn(models.Model):
    number = models.IntegerField()
    match = models.ForeignKey(Match)

    class Meta:
        # Don't want two turns with the same number in the same match
        unique_together = ('number', 'match')


class Action(models.Model):
    action = models.CharField(max_length=20, unique=True)

    def __unicode__(self):
        return self.action


class Shot(models.Model):
    number = models.IntegerField()
    turn = models.ForeignKey(Turn)
    action = models.ForeignKey(Action)
    balls = models.ManyToManyField(Ball)

    class Meta:
        # Don't want two shots with the same number in the same turn
        unique_together = ('number', 'turn')

    def get_match(self):
        return self.turn.match
