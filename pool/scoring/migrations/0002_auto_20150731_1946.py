# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def add_balls(apps, schema_editor):
    Ball = apps.get_model("scoring", "Ball")

    for x in range(1, 16):
        Ball.objects.create(number=x)


def add_actions(apps, schema_editor):
    Action = apps.get_model("scoring", "Action")

    for x in ["make", "miss", "foul", "safety"]:
        Action.objects.create(action=x)


def add_skill_levels(apps, schema_editor):
    SkillLevel = apps.get_model("scoring", "SkillLevel")

    levels = [
        [1, 14],
        [2, 19],
        [3, 25],
        [4, 31],
        [5, 38],
        [6, 46],
        [7, 55],
        [8, 65],
        [9, 75]
    ]

    for level in levels:
        SkillLevel.objects.create(level=level[0], handicap=level[1])


class Migration(migrations.Migration):

    dependencies = [
        ('scoring', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_balls),
        migrations.RunPython(add_actions),
        migrations.RunPython(add_skill_levels),
    ]
