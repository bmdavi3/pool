# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-14 23:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scoring', '0002_auto_20150731_1946'),
    ]

    operations = [
        migrations.AlterField(
            model_name='action',
            name='action',
            field=models.CharField(max_length=20, unique=True),
        ),
        migrations.AlterField(
            model_name='ball',
            name='number',
            field=models.IntegerField(unique=True),
        ),
        migrations.AlterField(
            model_name='skilllevel',
            name='level',
            field=models.IntegerField(unique=True),
        ),
    ]
