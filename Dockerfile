############################################################
# Dockerfile to build pool container images
# Based on Ubuntu
############################################################
FROM ubuntu:16.04

MAINTAINER Brian Davis

RUN apt-get update && apt-get -y upgrade

RUN apt-get install -y python-pip
RUN apt-get install -y postgresql-server-dev-9.5

ADD requirements.txt /var/www/pool/

WORKDIR /var/www/pool/

RUN pip install --upgrade pip
RUN pip install --requirement requirements.txt

ADD . /var/www/pool/

CMD gunicorn -c gunicorn_conf.py --chdir pool pool.wsgi:application --reload

# To build
# docker build -t pool .

# To run
# docker run -p 8000:8000 --env-file test.env -i -t pool
