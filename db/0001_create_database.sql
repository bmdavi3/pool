-- sudo -u postgres psql < db/0001_create_database.sql

-- You'll probably need to do this
-- /etc/postgresql/9.3/main/postgresql.conf
--   - listen_addresses = '*'

-- You probably aslo want to make a ~/.pgpass file, chmod 600 it, and put this in it
-- localhost:5432:pool:pool_user:pool_user_pass
-- localhost:5432:pool:pool_user_ro:pool_user_ro_pass
-- localhost:5432:pool:pool_user_rw:pool_user_rw_pass



DROP DATABASE IF EXISTS pool;

DROP USER IF EXISTS pool_user;
DROP USER IF EXISTS pool_user_ro;
DROP USER IF EXISTS pool_user_rw;

CREATE ROLE pool_user WITH LOGIN PASSWORD 'pool_user_pass';
CREATE ROLE pool_user_ro WITH LOGIN PASSWORD 'pool_user_ro_pass';
CREATE ROLE pool_user_rw WITH LOGIN PASSWORD 'pool_user_rw_pass';

CREATE DATABASE pool OWNER pool_user;

\c pool

CREATE SCHEMA pool AUTHORIZATION pool_user;

ALTER USER pool_user SET search_path to pool, public;
ALTER USER pool_user_ro SET search_path to pool, public;
ALTER USER pool_user_rw SET search_path to pool, public;

GRANT USAGE ON SCHEMA pool TO pool_user_ro, pool_user_rw;
ALTER DEFAULT PRIVILEGES FOR ROLE pool_user IN SCHEMA pool GRANT SELECT ON TABLES TO pool_user_ro, pool_user_rw;
ALTER DEFAULT PRIVILEGES FOR ROLE pool_user IN SCHEMA pool GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO pool_user_rw;
ALTER DEFAULT PRIVILEGES FOR ROLE pool_user IN SCHEMA pool GRANT USAGE ON SEQUENCES TO pool_user_rw;
